package fi.metropolia.translatorskeleton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import fi.metropolia.translatorskeleton.model.UserData;
import fi.metropolia.translatorskeleton.model.MyModelRoot;
import fi.metropolia.translatorskeleton.model.User;
import fi.metropolia.translatorskeleton.model.Dictionary;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UserData u = MyModelRoot.getInstance().getUserData();
        u.setUser(new User("Myself"));
        u.addDictionary("fineng", new Dictionary("fin", "eng"));
        u.addDictionary("engfin", new Dictionary("eng", "fin"));
    }
}
